#### Поднимаем gitlab-runner
```shell script
cd ~/project
# Создаём структуру папок
sudo mkdir -p /srv/gitlab-runner/{cache,config,jobs}
sudo docker-compose -f docker-compose.staging.yml -p staging up -d gitlab-runner
# Или вручную:
sudo docker run -d --restart always --name gitlab-runner \
  -v /srv/gitlab-runner/config:/etc/gitlab-runner \
  -v /var/run/docker.sock:/var/run/docker.sock \
  gitlab/gitlab-runner:latest
```

#### Регистрируем gitlab-runner
Небольшое отличие от **Production**:  
Монтирование `/srv/gitlab-runner/shared_jobs/` пригодится, чтобы можно было вытаскивать данные из контейнеров
после тестов (например, скриншоты с тестов **Selenium** и др. артефакты).
```shell script
sudo docker run --rm -v /srv/gitlab-runner/config:/etc/gitlab-runner gitlab/gitlab-runner:latest register \
  --non-interactive \
  --executor "docker" \
  --docker-image "docker:stable" \
  --docker-volumes "/var/run/docker.sock:/var/run/docker.sock" \
  --docker-volumes "/srv/gitlab-runner/shared_jobs:/shared_jobs" \
  --docker-volumes "/srv:/srv" \
  --url "https://gitlab.company.ru" \
  --registration-token "<PROJECT_REGISTRATION_TOKEN>" \
  --description "Staging server gitlab runner" \
  --tag-list "staging" \
  --run-untagged="false" \
  --locked="false" \
  --access-level="not_protected"
# https://habr.com/ru/post/449910
# Если машина, которая будет собирать и тестировать, мощная, можно добавить параллельных сборок
sudo nano /srv/gitlab-runner/etc/gitlab-runner/config.toml
#######################################
concurrent=20
[[runners]]
 request_concurrency = 10
#######################################

```