Монорепозиторий приложения с тремя ветками *feature*-ветки, *master* и *development* и двумя сабмодулями 
restaurant-submodule-web && restaurant-submodule-ui-kit-react(git submodule update --recursive)
В *development* ведется интеграция всех *feature*-веток, которые удаляются после успешного слияния. Стабильные релизы идут в *master*.
Traefik собирает информацию о запущенных динамических DNS-именах для **.dev.company.ru*,
подключившись к докеру по TCP и перенаправляет на доменные имена сервисов. Wildcard сертификат **dev.company.ru* получается с помощью отдельного контейнера (production.letsencrypt-dns).
Gitlab работает через 1023 порт(SSH)

* Docker-compose устанавливаем ТОЛЬКО официальным методом, иначе можно влететь в ошибку (ссылка на решение в Issue):
https://github.com/docker/compose/issues/6023#issuecomment-427896175
* Docker-compose будем запускать в контейнере с помощью официального скрипта-обёртки:
https://docs.docker.com/compose/install/#install-as-a-container
```shell script
# установка docker
curl -fsSL https://get.docker.com -o get-docker.sh && sudo sh get-docker.sh
# Переход на overlay2
# https://docs.docker.com/storage/storagedriver/overlayfs-driver/
sudo touch /etc/docker/daemon.json && sudo nano /etc/docker/daemon.json
#######################################
# добавить текст в файл
{
  "storage-driver": "overlay2"
}
#######################################
sudo systemctl restart docker
# docker-compose
sudo curl -L --fail https://github.com/docker/compose/releases/download/1.25.3/run.sh -o /usr/local/bin/docker-compose \
&& sudo chmod +x /usr/local/bin/docker-compose
# Установка временной зоны
sudo dpkg-reconfigure tzdata
sudo unlink /etc/localtime && sudo ln -s /usr/share/zoneinfo/Europe/Volgograd /etc/localtime
```

#### Доступ к docker по TCP с TLS сертификатом
* Пример для [**Staging**] - там необходимо это проделать для автоматического подтягивания динамических окружений
в **Traefik**. Для остальных серверов можно сделать только для удобства менеджмента (например, Portainer или в IDE).
Для этого нужно заменить IP адрес и путь в конце.
* Выполняем по гайду (по нему придется раз в год обновлять, можно увеличить количество *-days 365*):
https://docs.docker.com/engine/security/https/  
     * Когда напарываемся, что нужен *.RND* файл:
    ```shell script
    # https://crypto.stackexchange.com/questions/68919/is-openssl-rand-command-cryptographically-secure
    openssl rand -hex 32 > ~/.rnd
    ```
    Для того, чтобы в итоге запустить dockerd с сертификатом, необходимо перенастроить запуск с помощью systemd:
    * https://docs.docker.com/engine/reference/commandline/dockerd/#daemon-socket-option
    * https://docs.docker.com/engine/reference/commandline/dockerd/#daemon-configuration-file
    * https://docs.docker.com/config/daemon/systemd/#custom-docker-daemon-options
    * https://docs.docker.com/install/linux/linux-postinstall/#configuring-remote-access-with-systemd-unit-file

##### Итоговая последовательность команд
```shell script
mkdir docker-certs \
&& cd docker-certs \
&& openssl genrsa -aes256 -out ca-key.pem 4096
#######################################
# запомнить, вводить в дальнейшем по запросам
passphrase: *****
#######################################

openssl req -new -x509 -days 365 -key ca-key.pem -sha256 -out ca.pem
#######################################
# ввод данных
RU
Volgograd-district
Volgograd
Company LLC
IT
192.168.88.3
mail@company.ru
#######################################

openssl genrsa -out server-key.pem 4096 \
&& cd ~ \
&& openssl rand -hex 32 > ~/.rnd \
&& cd docker-certs \
&& openssl req -subj "/CN=192.168.88.3" -sha256 -new -key server-key.pem -out server.csr \
&& echo subjectAltName = DNS:192.168.88.3,IP:192.168.88.3 >> extfile.cnf \
&& echo extendedKeyUsage = serverAuth >> extfile.cnf \
&& openssl x509 -req -days 365 -sha256 -in server.csr -CA ca.pem -CAkey ca-key.pem \
  -CAcreateserial -out server-cert.pem -extfile extfile.cnf

openssl genrsa -out key.pem 4096 \
&& openssl req -subj '/CN=client' -new -key key.pem -out client.csr \
&& echo extendedKeyUsage = clientAuth > extfile-client.cnf \
&& openssl x509 -req -days 365 -sha256 -in client.csr -CA ca.pem -CAkey ca-key.pem \
  -CAcreateserial -out cert.pem -extfile extfile-client.cnf

rm -v client.csr server.csr extfile.cnf extfile-client.cnf
# Скопировать ca.pem cert.pem key.pem для подключения клиента.
cd ~ \
&& mkdir .docker \
&& sudo mv docker-certs/* .docker/ \
&& rm -rf docker-certs
sudo systemctl edit docker.service
#######################################
# добавить в файл
[Service]
ExecStart=
ExecStart=/usr/bin/dockerd -H fd:// -H tcp://0.0.0.0:2376 --tlsverify --tlscacert=/home/<your_name>/.docker/ca.pem --tlscert=/home/<your_name>/.docker/server-cert.pem --tlskey=/home/<your_name>/.docker/server-key.pem
################esli nety PID1###################
cd /etc/docker/daemon.json
"hosts": ["unix:///var/run/docker.sock", "tcp://0.0.0.0:2376"],
"tlscacert": "/home/<your_name>/.docker/certs/ca.pem",
"tlscert": "/home/<your_name>/.docker/certs/server-cert.pem",
"tlskey": "/home/<your_name>/.docker/certs/server-key.pem",
"tlsverify": true
###############################################################
sudo systemctl daemon-reload && sudo systemctl restart docker.service \
&& sudo chmod 600 ~/.docker/* \
&& rm ~/.rnd
```


### GitLab
#### Установка с помощью docker-compose в докере
* https://docs.gitlab.com/omnibus/docker/#install-gitlab-using-docker-compose  
    Необходимо перекинуть файл `docker-compose.services.yml` на [**Services**].
    ```shell script
    # Создаём структуру папок
    sudo mkdir -p /srv/services/gitlab/{config,data,logs}
    sudo docker-compose -f docker-compose.services.yml -p gitlab up -d
    ```

#### Установка GitLab Omnibus прямо на машину
* Конфигурация **GitLab** со своим **Registry** для работы c SSL reverse-proxy:
```shell script
sudo nano /etc/gitlab/gitlab.rb
#######################################
external_url "https://gitlab.company.ru"
nginx['proxy_set_headers'] = {
  "Host" => "$http_host",
  "X-Real-IP" => "$remote_addr",
  "X-Forwarded-For" => "$proxy_add_x_forwarded_for",
  "X-Forwarded-Proto" => "https",
  "X-Forwarded-Ssl" => "on"
}
nginx['listen_port'] = 80
nginx['listen_https'] = false
gitlab_rails['time_zone'] = 'Europe/Volgograd'
gitlab_rails['trusted_proxies'] = ['192.168.88.1', '192.168.88.2']
registry_external_url 'https://registry.company.ru'
registry['enable'] =true
registry_nginx['enable'] = true
registry_nginx['proxy_set_headers'] = {
  "Host" => "$http_host",
  "X-Real-IP" => "$remote_addr",
  "X-Forwarded-For" => "$proxy_add_x_forwarded_for",
  "X-Forwarded-Proto" => "https",
  "X-Forwarded-Ssl" => "on"
}
registry_nginx['listen_port'] = 5005
registry_nginx['listen_https'] = false
nginx['real_ip_trusted_addresses'] = ['192.168.88.1', '192.168.88.2']
nginx['real_ip_header'] = 'X-Forwarded-For'
nginx['real_ip_recursive'] = 'on'
sudo gitlab-ctl reconfigure
# nginx требует отдельного перезапуска
sudo gitlab-ctl hup nginx
```

### Production
#### Поднимаем Traefik
Пока мы не подняли реверс-прокси, если зайти на **GitLab** напрямую по IP адресу, то 
он все запросы редиректит на https, согласно нашим настройкам, сертификата которого пока нет.
* На [**Production**] необходимо переместить папку `production` и `docker-compose.prod.yml`
из примера, а также TLS сертификаты к докеру [**Staging**].
Предполагаем, что поместили всё в папку `/usr/src/app` на сервере
```shell script
cd ~/project
# Создаём структуру папок для возможности скопировать файлы далее
sudo mkdir -p /srv/{gitlab-runner/config,demo-prj/{backend/media,db/data},letsencrypt-dns,traefik/{certs/staging,letsencrypt,logs}}
# перекидываем файлы конфигураций traefik
sudo cp -r ~/project/production/* /srv/
sudo docker-compose -f docker-compose.prod.yml -p prod up -d traefik
#######################################
# Или можно вручную для тестов:
sudo docker pull traefik:v2
# шаг, если обновляем вручную:
# sudo docker stop traefik && sudo docker rm traefik
sudo docker run -d \
    --restart always \
	--name traefik \
	--volume /srv/traefik/traefik.yml:/etc/traefik/traefik.yml \
	--volume /srv/traefik/dynamic-conf.yml:/etc/traefik/dynamic-conf.yml \
	--volume /srv/traefik/letsencrypt:/letsencrypt \
	--volume /srv/traefik/certs:/certs \
	--volume /srv/traefik/logs:/logs \
	--volume /usr/share/zoneinfo:/usr/share/zoneinfo:ro \
	-p 8080:8080 \
	-p 80:80 \
	-p 443:443 \
	-p 1023:1023 \
	-e TZ=Europe/Volgograd \
	traefik:latest
#######################################
```

* На этом этапе нужно зайти на **GitLab** и получить токен проекта из Settings->CI/CD.
* При первом входе на **GitLab** нужно указать пароль пользователя `root`, есть проверка на сложность.
* Заодно нужно установить в проекте переменные
  * $DNS_PROVIDER_API_TOKEN и $LETSENCRYPT_EMAIL для контейнера letsencrypt-dns (см. ниже) или другие, для тех же целей у **Traefik**.
  * $TZ, https://gitlab.com/gitlab-com/support-forum/issues/4051

#### Поднимаем gitlab-runner
```shell script
cd ~/project
sudo docker-compose -f docker-compose.prod.yml -p prod up -d gitlab-runner
# Или вручную:
sudo docker run -d --restart always --name gitlab-runner \
  -v /srv/gitlab-runner/config:/etc/gitlab-runner \
  -v /var/run/docker.sock:/var/run/docker.sock \
  gitlab/gitlab-runner:latest
```
#### Регистрируем gitlab-runner
* Доступ к `/srv` нужен, если захотим управлять другими приложениями прямо из проекта на **GitLab**
(например, удалить неисправную БД или обновить версию **Traefik**)
* Расшаривание `/var/run/docker.sock` позволяет запускать контейнеры на хосте командой `docker`
из раннера, самого запущенного в Docker
```shell script
# Конфигурируем раннер с помощью другого контейнера, который будет запущен один раз.
# Требуется docker.sock для использования в .gitlab-ci.yml образа с docker и docker-compose:
# https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#use-docker-socket-binding
sudo docker run --rm -v /srv/gitlab-runner/config:/etc/gitlab-runner gitlab/gitlab-runner:latest register \
  --non-interactive \
  --executor "docker" \
  --docker-image "docker:stable" \
  --docker-volumes "/var/run/docker.sock:/var/run/docker.sock" \
  --docker-volumes "/srv:/srv" \
  --url "https://gitlab.company.ru/" \
  --registration-token "<PROJECT_REGISTRATION_TOKEN>" \
  --description "Production server gitlab runner" \
  --tag-list "production" \
  --run-untagged="false" \
  --locked="false" \
  --access-level="not_protected"
```

#### Опционально letsencrypt-dns
Нужен для получения WC сертификата для динамических dns-имен тестируемых сборок.  
Уже можно развернуть автоматом через **GitLab**, если создан проект с инфраструктурой.

Для поднятия в тестовом режиме вручную:  
```shell script
# -d - detached режим, если устанавливаем время ожидания обновления DNS-записей большим
# (например, вы используете отечественного DNS-провайдера как Яндекс)
sudo docker run -d \
    --name letsencrypt-dns \
    --restart always \
    --volume /srv/letsencrypt-dns:/etc/letsencrypt \
    --env 'LETSENCRYPT_USER_MAIL=<почта пользователя letsencrypt>' \
    --env 'LEXICON_PROVIDER=yandex' \
    --env 'LEXICON_SLEEP_TIME=1800' \
    --env 'LEXICON_PROVIDER_OPTIONS=--auth-token=<токен провайдера DNS>' \
    --env 'LETSENCRYPT_STAGING=true' \
    --env 'TZ=Europe/Volgograd' \
    adferrand/letsencrypt-dns:latest
# Смотрим /srv/letsencrypt-dns/certs/ - если всё в порядке, появятся сертификаты.
# Тогда очистить папки, перезапустить контейнер, убрав переменную LETSENCRYPT_STAGING.
# Теперь сертификаты будут получены настоящие.
# Staging сервера Let's Encrypt используются, чтобы не тратить количество выпусков сертификатов
# https://letsencrypt.org/docs/rate-limits/ (wildcard 5 раз в неделю можно запросить)
# Логи смотреть sudo docker logs letsencrypt-dns
```
Полученные сертификат и приватный ключ переместить в папку `/srv/traefik/certs`,
а также переименовать в *dev.company.ru.cert.pem* и *dev.company.ru.privkey.pem* (см. *dynamic-conf.yml*).
После этого необходимо перезапустить **Traefik**, чтобы он подхватил сертификаты
```shell script
sudo cp /srv/letsencrypt-dns/live/dev.company.ru/cert.pem /srv/traefik/certs/dev.company.ru.cert.pem && sudo cp /srv/letsencrypt-dns/live/dev.company.ru/privkey.pem /srv/traefik/certs/dev.company.ru.privkey.pem
sudo docker restart traefik
```
#### Отключение портфорвардинга(точнее его запрет) в WSL

$remoteport = bash.exe -c "ifconfig eth0 | grep 'inet '"
$found = $remoteport -match '\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}';

if( $found ){
  $remoteport = $matches[0];
} else{
  echo "The Script Exited, the ip address of WSL 2 cannot be found";
  exit;
}

#[Ports]

#All the ports you want to forward separated by coma
$ports=@(80,443,10000,3000,5000);


#[Static ip]
#You can change the addr to your ip config to listen to a specific address
$addr='0.0.0.0';
$ports_a = $ports -join ",";


#Remove Firewall Exception Rules
iex "Remove-NetFireWallRule -DisplayName 'WSL 2 Firewall Unlock' ";

#adding Exception Rules for inbound and outbound Rules
iex "New-NetFireWallRule -DisplayName 'WSL 2 Firewall Unlock' -Direction Outbound -LocalPort $ports_a -Action Allow -Protocol TCP";
iex "New-NetFireWallRule -DisplayName 'WSL 2 Firewall Unlock' -Direction Inbound -LocalPort $ports_a -Action Allow -Protocol TCP";

for( $i = 0; $i -lt $ports.length; $i++ ){
  $port = $ports[$i];
  iex "netsh interface portproxy delete v4tov4 listenport=$port listenaddress=$addr";
  iex "netsh interface portproxy add v4tov4 listenport=$port listenaddress=$addr connectport=$port connectaddress=$remoteport";
}

#### Статьи использованные для: 
Elasticsearch SSL - https://aws.amazon.com/blogs/opensource/add-ssl-certificates-open-distro-for-elasticsearch/ <br /> 
Кубов - https://developer.cisco.com/codeexchange/github/repo/juliogomez/devops/ <br />
            - https://dev.to/bowmanjd/install-docker-on-windows-wsl-without-docker-desktop-34m9 <br />
            - https://docs.docker.com/engine/install/ubuntu/ <br />
            - https://medium.com/@aujlanavi0991/windows-subsystem-for-linux-wsl2-override-dns-configuration-ae4207b1a7f7 <br />
            - https://www.abhishek-tiwari.com/local-development-environment-for-kubernetes-using-minikube/ <br />
            - https://v1-16.docs.kubernetes.io/docs/setup/learning-environment/minikube/ <br />
Снипетов Helm - https://cloud.croc.ru/blog/byt-v-teme/kubernetes-helm-chart/ <br />
Ingress - https://medium.com/kubernetes-tutorials/deploying-traefik-as-ingress-controller-for-your-kubernetes-cluster-b03a0672ae0c <br />
