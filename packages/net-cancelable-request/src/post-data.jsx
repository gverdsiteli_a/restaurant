/* eslint-disable no-alert */
import { useEffect, useState, useRef} from 'react';


export function PostData({id = null,  ...options} = {}){
  const [data, setData] = useState(null);
  //Место для хранения свежайщего AbortController`a
  const lastAbortController = useRef();
  useEffect(()=>{
    Object.assign(options, { id });
    //const otherProps = { ...options }; если нужно передать все кроме id
    setData(null);
    if(lastAbortController.current){ //если есть инстанц класса AbortController
      lastAbortController.current.abort()
    }
    const curAbortController = new AbortController()
    lastAbortController.current = curAbortController

    const curPromise = fetch(`http://jsonplaceholder.typicode.com/todos/${options.id}`, {
      signal:curAbortController
    })
    .then(res => res.json())
    .then(data => {
      if(curAbortController.signal.aborted){
        return new Promise(()=>{})
      }
      return data;
    })
    curPromise.then(res => {
      if(abortController.signal.aborted){
        return;
      }
      setData(res), 
      e => console.warn('fetch failure', e)
    })
    return () => abortController.abort(); 
  },[id])
  console.log(options);
  return (
    <div>
      { data ? (
        <div>
          <h2>{data.id}</h2>
          {data.title}
        </div>
      ) : null
    }
    </div>
  )
}