module.exports = {
    FEEDBACK_CREATED: "*.*.feedback.created",
    FEEDBACK_UPDATED: "*.*.feedback.updated",
    FEEDBACK_REMOVED: "*.*.feedback.removed",
};
