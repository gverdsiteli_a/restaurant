const course = require("./course");
const user = require("./user");
const feedback = require("./feedback");
module.exports = {
    ...course,
    ...user,
    ...feedback,
};
