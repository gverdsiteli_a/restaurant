## Usage
const crypto = require('@restaurant/crypto')

const password = 'Super-secure-password'

crypto.hash.hash(password)
    .then(hash => crypto.hash.compare(password, hash))
    .then(isEqual => console.log('Are passwords equal?', isEqual))
    .catch(console.error)

