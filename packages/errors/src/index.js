const business = require("./business");
const colors = require("./colors");
const rfc = require("./rfc");
const status = require("./status")


module.exports = {
    ...business,
    ...colors,
    ...rfc,
    ...status
};
