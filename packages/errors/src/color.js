module.exports = {
    BLACK_LOG:"\x1b[30m%s\x1b[0m",
    RED_LOG:"\x1b[31m%s\x1b[0m",
    GREEN_LOG:"\x1b[32m%s\x1b[0m",
    YELLOW_LOG:"\x1b[33m%s\x1b[0m",
    BLUE_LOG:"\x1b[34m%s\x1b[0m",
    MAGENTA_LOG:"\x1b[35m%s\x1b[0m",
    CYAN_LOG:"\x1b[36m%s\x1b[0m",
    WHITE_LOG:"\x1b[37m%s\x1b[0m"
}

