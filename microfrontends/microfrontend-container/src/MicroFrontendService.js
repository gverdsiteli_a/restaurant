import loadjs from 'loadjs';

const loadedCache = {};

const loadMicroFrontend = async (host, callback) => {
  const cached = loadedCache[host];
  if (cached) {
    callback();
    return;
  }
  const res = await fetch(`${host}/asset-manifest.json`);
  const json = await res.json();
  loadedCache[host] = true;

  const entrypoints = json.entrypoints;
  if (!entrypoints) {
    callback();
    return;
  }
  const files = [];
  for (let i = 0; i < entrypoints.length; i++) {
    const fileName = `${host}/${entrypoints[i]}`;
    const isLoaded = document.getElementById(fileName);
    if (!isLoaded) {
      files.push(fileName);
    }
  }
  if (files.length < 1) {
    callback();
    return;
  }
  loadjs(files, {
    async: true,
    success: callback,
    before: (path, scriptEl) => {
      scriptEl.id = path;
    }
  });
};

export default loadMicroFrontend;