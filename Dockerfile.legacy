FROM node:15.13.0-alpine3.10 as builder
ARG SERVICE
WORKDIR /usr/src/app

COPY --chown=node:node package*.json lerna.json ./
COPY --chown=node:node packages/ ./packages
COPY --chown=node:node staging/ ./staging/
COPY --chown=node:node services/${SERVICE} ./services/${SERVICE}
RUN npm install --loglevel notice --unsafe-perm

ENV NODE_ENV=production \
    PORT=3000 \
    SERVICE_NAME=${SERVICE}

HEALTHCHECK --interval=30s \
    --timeout=2s \
    --retries=10 \
    CMD node services/${SERVICE_NAME}/src/healthcheck.js

EXPOSE 3000

CMD ["npm", "--prefix", "services/${SERVICE}", "start"]

FROM alpine as submodules
RUN apt-get update && apt-get install shadow git && apt-get cache clean
RUN addgroup -g 2000 node \
    && adduser -u 2000 -G node -s /bin/sh -D node
RUN usermod -u $UID node \
&& groupmod -g $GIU node
USER 2000
WORKDIR /usr/src/app
COPY --chown=node:node --from=builder /usr/src/app .
WORKDIR /usr/src/app/production
RUN rm -rf web
RUN git clone https://gitlab.com/gverdsiteli_a/restaurant-submodule-web web
USER node
RUN cd web && \
    npm i --silent --loglevel notice --unsafe-perm && \
    pm2 start npm --name "web" --run dev --no-daemon
#RUN git clone RUN git clone https://gitlab.com/gverdsiteli_a/restaurant-submodule-ui-kit-react ui-kit
#RUN cd ui-kit && \
#    npm i --silent --loglevel notice --unsafe-perm && \
#    pm2 start npm --name "ui-kit" --run dev --no-daemon
